<div class="breadcrumb">
  <a href="coding.php">Coding Projects</a> &gt; JavaScript Microwave
</div>
<div class="main-content">
  <h2>JavaScript Microwave</h2>
  <p>
    This is a class assignment using JavaScript and CSS to create a countdown timer with a microwave interface. The CSS styling for the microwave was done by a classmate and the JavaScript that makes it functional was done by myself.
  </p>
  <p>
    The microwave displays the time (in 24hr format) when there is no input entered. All of the buttons function as a real microwave would. Feel free to test it out!
  </p>
</div>
<iframe class="iframeStyle" src="http://eliasnahas.com/javascript/microwave/index.html"></iframe>
