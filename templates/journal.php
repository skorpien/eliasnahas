<div class="breadcrumb">
  <a href="coding.php">Coding Projects</a> &gt; PHP/MySQL Journal
</div>
<div class="main-content">
  <h2>PHP/MySQL Journal</h2>
  <p>
    In this class assignment, we had to use PHP and MySQL to create a fully functioning online journal. It had to include a way for a user to create an account, be able to log in with the proper credentials, and only be able to see his or her own journal entries.
  </p>
  <p>
    We also used RegEx validation to ensure the password that was used is a strong password and that the email address for the account was valid. The app also sterilizes any user input as to prevent SQL injection into the database.
  </p>
  <p>
    Feel free to click around and create an account, but please keep in mind that this is just a class assignment. Do not include any sensitive information. I do not collect nor sell any of your information &ndash; the email address is not verified so please use a made up one for your own security.
  </p>
  <p>Reset the database by <a href="http://eliasnahas.com/verify/reset.php" target="iframe_verify">clicking here</a>.
</div>
<iframe class="iframeStyle" name="iframe_verify" src="http://eliasnahas.com/verify/login.php"></iframe>
