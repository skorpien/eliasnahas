<div class="main-content">
  <h2>Contact Me</h2>
  <p>
    If you want to get in touch with me, you can do so at the following:<br>
    email: <a href="mailto:elias.nahas@gmail.com">elias.nahas@gmail.com</a><br>
    phone: <a href="tel:4036168912">(403) 616-8912</a>
  </p>
  <p>
    Or you can contact me via the form below.
  </p>
  <?php
  $fieldsArr = ['name', 'email', 'item'];
  if( formWasSubmitted() ) {
    validateForm($fieldsArr);
  }
  ?>

  <form method="post" class="contactForm">
    <table class="fullWidth">
      <tr>
        <td class="alignRight">*Name: </td>
        <td>
          <input type="text" name="name" class="fullWidth" <?php echo repopulateIfSubmitted('name'); ?> />
        </td>
        <td class="error">*Denotes required field</td>
      </tr>
      <tr>
        <td class="alignRight">*Email: </td>
        <td>
          <input type="text" name="email" class="fullWidth" <?php echo repopulateIfSubmitted('email'); ?> />
        </td>
        <td></td>
      </tr>
      <tr>
        <td class="alignRight alignTop">*Message: </td>
        <td colspan="2">
          <textarea name="item" class="fullWidth fullHeight"><?php echo repopulateIfSubmitted('item'); ?></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3" style="text-align:right;">
          <button type="submit" name="Submit">Submit</button>
        </td>
      </tr>
    </table>
    <div class="g-recaptcha fullWidth" data-sitekey="6Lf2rR4TAAAAAParyH5tdDpmUnNp5ENAnacm2T2D"></div>
  </form>
  <div class="contactSuccess">Success! Your message has been sent.</div>
</div>
