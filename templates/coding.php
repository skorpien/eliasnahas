<div class="main-content">
  <h1>Coding Projects</h1>
  <p>
    In this section, I've included two class assignments &ndash; one using JavaScript and one using PHP and MySQL.
  </p>
  <p>
    In addition to these two assignments, I have a github repository which has more recent projects and tutorials. You can view my repository here: <a href="https://github.com/eliasnahas" target="_blank">https://github.com/eliasnahas</a>
  </p>
  <div class="project">
    <h2>JavaScript Microwave</h2>
    <a href="microwave.php"><img src="images/thumb_microwave.jpg" alt="JavaScript Microwave"></a><br>
    <span class="caption">A JavaScript countdown timer</span>
  </div>
  <div class="project">
    <h2>PHP/MySQL Journal</h2>
    <a href="journal.php"><img src="images/thumb_verify.jpg" alt="PHP/MySQL Journal"></a><br>
    <span class="caption">A PHP/MySQL journal app with<br>
      registration and login</span>
  </div>
  <br class="brClear">
</div>
