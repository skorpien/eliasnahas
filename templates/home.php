<div class="main-content">
  <h1>A little about me</h1>
  <p>
    Welcome! I'm a Calgary based web developer with a background in graphic design and a knack for all things web. I'm well versed in the internet's most popular programming languages and am always eager to learn more.
  </p>
  <p>
    I have recently completed the Web Developer Fast Track program at SAIT and am looking for employment doing what I love to do. Over the length of my course, I have completed multiple class assignments &ndash; both individually and within a group &ndash; some of which I have included in my online portfolio.
  </p>
  <p>
    Please feel free to take a look, click around, and interact with my projects.
  </p>
</div>
