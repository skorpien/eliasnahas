<div class="main-content">
  <h1>Website Projects</h1>
  <p>
    Here are a few examples of some websites that I've made for in-class assignments. The majority use WordPress as a Content Management System, and in the case of the City of Brooks website, custom PHP to the WordPress theme to add additional functionality.
  </p>
  <p>
    As well, the City of Brooks site had to be converted from a WordPress website residing locally to one that is hosted online. The original class project was hosted locally rather than using a webhost. This meant that the MySQL database had to be altered to make all of the posts, pages and images appear as they should.
  </p>
  <div class="project">
    <h2>City of Brooks<br>
      Website Redesign</h2>
      <p>(Group Assignment)</p>
    <a href="http://eliasnahas.com/brooks/" target="_blank"><img src="images/thumb_brooks.jpg" alt="City of Brooks Website Redesign"></a><br>
    <span class="caption">Redesign of City of Brooks website<br>
      using WordPress</span>
  </div>
  <div class="project">
    <h2>Sechelt Visitor Centre<br>
      Website Redesign</h2>
      <p>(Individual Assignment)</p>
    <a href="http://eliasnahas.com/sechelt/" target="_blank"><img src="images/thumb_sechelt.jpg" alt="Sechelt Visitor Centre Website Redesign"></a><br>
    <span class="caption">Redesign of Sechelt Visitor Centre<br>
      website using WordPress</span>
  </div>
  <br class="brClear">
  <div class="project">
    <h2>CG Law LLP</h2>
      <p>(Individual Assignment)</p>
    <a href="http://eliasnahas.com/cglaw/" target="_blank"><img src="images/thumb_cglaw.jpg" alt="CG Law LLP Website"></a><br>
    <span class="caption">A fictitious website for a law firm<br>
    using WordPress</span>
  </div>
  <div class="project">
    <h2>ABC Painting Website</h2>
      <p>(Individual Assignment)</p>
    <a href="http://eliasnahas.com/abc-painting/" target="_blank"><img src="images/thumb_abcpainting.jpg" alt="ABC Painting Website"></a><br>
    <span class="caption">A fictitious house painting company<br>
      website using HTML5 and CSS3</span>
  </div>
  <br class="brClear">
</div>
