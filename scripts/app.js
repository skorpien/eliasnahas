$(function() {
  $('.mobile-menu').click(function() {
    $('.nav-menu').slideToggle(200);
    var mobileIcon = $(this).text();
    if (mobileIcon !== 'x') {
      $(this).text('x');
    } else {
      $(this).text('≡');
    }
  });

  $(window).resize(function(){
       var w = $(window).width();
       var menu = $('.nav-menu');
       if(w > 320 && menu.is(':hidden')) {
           menu.removeAttr('style');
     }
   });

});
