<?php

function formWasSubmitted() {
  return !empty($_POST);
}

function elementWasMissing($element) {
  return empty($_POST[$element]);
}

function printError($error) {
  return '<span class="error">'.$error.'</span>';
}

function validateRegex($fieldName) {
  if (!elementWasMissing($fieldName)) {
    $fieldValue = $_POST[$fieldName];

    $emailRegex = '/^[a-zA-Z0-9\.-_]+@[a-zA-Z0-9\.-_]+\.[a-zA-Z]{2,4}$/';

    switch ($fieldName) {
      case 'email':
        if (preg_match($emailRegex, $fieldValue)) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return false;
        break;
    }
  }
}

function validateField($field, $fieldsStr) {
  if (preg_match("'$field'", $fieldsStr)) {
    return true;
  } else {
    return false;
  }
}

function validateForm($fieldsArr) {
  $fieldsStr = '';
  foreach ($fieldsArr as $value) {
    $fieldsStr .= $value;
  }

  if(formWasSubmitted()) {
    if (elementWasMissing('name') && validateField('name', $fieldsStr)) {
      echo printError("Please enter your name.");
    } elseif (elementWasMissing('email') && validateField('email', $fieldsStr)) {
      echo printError("Please enter an email address.");
    } elseif (!elementWasMissing('email') && !validateRegex('email')) {
      echo printError("Please enter a valid email address.");
    } elseif (elementWasMissing('item') && validateField('item', $fieldsStr)) {
      echo printError("Please enter a message.");
    } else {
      formAction();
    }
  }
}

function repopulateIfSubmitted($fieldName) {
  if(isset($_POST[$fieldName]) && !empty($_POST[$fieldName])) {
    if($fieldName !== 'item') {
      return "value='$_POST[$fieldName]'";
    } else {
      return getValue('item');
    }
  }
}

function getValue($field) {
  return $_POST[$field];
}

function formAction() {
  $name = getValue('name');
  $email = getValue('email');
  $item = getValue('item');
  $to = 'elias.nahas@gmail.com';
  $message = 'From: '.$name."\r\n".'Reply-To: '.$email."\r\n".'Message: '.$item;
  $subject = 'Contact from Website';

  // your secret key
  $secret = "";

  // empty response
  $response = null;

  // check secret key
  $reCaptcha = new ReCaptcha($secret);

  if ($_POST["g-recaptcha-response"]) {
      $response = $reCaptcha->verifyResponse(
          $_SERVER["REMOTE_ADDR"],
          $_POST["g-recaptcha-response"]
      );
  }
  if ($response != null && $response->success) {
    mail($to, $subject, $message);
    header('Location: success.php');
  } else {
    echo printError("The reCAPTCHA box needs to be checked.");
  }
}
