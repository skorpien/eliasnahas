<html>
<head>
	<title>Elias Nahas | Web Developer - Contact Me</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="scripts/styles.css">
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <link rel="icon" href="favicon.ico">
</head>
<body>
	<?php include_once("analyticstracking.php") ?>
	<a class="skip-main" href="#main">Skip to main content</a>
  <div class="wrapper">
    <div class="container">
      <?php

      require_once 'scripts/recaptchalib.php';
      require_once 'functions/strings.php';
      require_once 'templates/header.php';
      require_once 'templates/contact.php';
      require_once 'templates/footer.php';

      ?>
    </div>
  </div>
  <script src="scripts/jquery.min.js"></script>
  <script src="scripts/app.js"></script>
</body>
</html>
